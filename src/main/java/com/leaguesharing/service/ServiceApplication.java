package com.leaguesharing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leaguesharing.service.model.dao.*;
import com.leaguesharing.service.model.entity.*;

@SpringBootApplication
@EnableAutoConfiguration
@RestController
public class ServiceApplication {
	@Autowired UserDAO userDAO;
	@Autowired RoleDAO roleDAO;
	@Autowired ArticleDAO articleDAO;
	@Autowired CategoryDAO categoryDAO;
	@Autowired PasswordEncoder passwordEncoder;
	@Autowired ImageArticleDAO imageArticleDAO;
	
	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}
	
	@RequestMapping(value="", method= RequestMethod.GET)
	String createDefaultData() {
		imageArticleDAO.deleteAll();
		articleDAO.deleteAll();
		categoryDAO.deleteAll();
		userDAO.deleteAll();
		roleDAO.deleteAll();
		
		Role premierRole = new Role("Particular");
		Role deuxiemeRole = new Role("Professional");
		User premierUser = new User(premierRole,"joedu60","joe@gmail.com",passwordEncoder.encode("12345"),"woman",0,"","/assets/image/chien-assis.jpg");

		Category decoration_lumineuse = new Category(null, "LIGHT_DECORATION");
		Category child0Decoration_lumineuse = new Category(decoration_lumineuse, "CHRISTMAS_TREE_AND_FIR_GARLAND"),
				 child1Decoration_lumineuse = new Category(decoration_lumineuse, "STALACTITE_GARLAND_AND_CURTAIN"),
				 child2Decoration_lumineuse = new Category(decoration_lumineuse, "CHARACTER_ANIMALS_AND_BRIGHT_OBJECT"),
				 child3Decoration_lumineuse = new Category(decoration_lumineuse, "LUMINOUS_PROJETCOR")	,
				 child4Decoration_lumineuse = new Category(decoration_lumineuse, "ELECTRIC_ACCESSORY");

		Article premierArticle = new Article(premierUser, "Guirlande lumineuse 10 m Multicouleur 100 LED CV", "", "Pour le sapin, autour de la crèche ou dans un vase transparent, la guirlande électrique est plus que nécessaire dans notre décoration lumineuse de Noël ! Alors n'hésitez pas, illuminez votre intérieur !", "9,99€", "offer", "sale",child0Decoration_lumineuse,"11 rue george danton",48.8795179,2.356569199999967);
		ImageArticle imageArticle = new ImageArticle("/assets/image/guirlande_lumineuse.jpg", premierArticle);
		categoryDAO.save(decoration_lumineuse);
		categoryDAO.save(child0Decoration_lumineuse);
		categoryDAO.save(child1Decoration_lumineuse);
		categoryDAO.save(child2Decoration_lumineuse);
		categoryDAO.save(child3Decoration_lumineuse);
		categoryDAO.save(child4Decoration_lumineuse);
		articleDAO.save(premierArticle);
		roleDAO.save(deuxiemeRole);
		imageArticleDAO.save(imageArticle);

		
		return "Creation bdd ok";
	}
}
