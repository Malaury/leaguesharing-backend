package com.leaguesharing.service.controller;

import com.leaguesharing.service.model.dto.ImageArticleDTO;
import com.leaguesharing.service.model.entity.ImageArticle;
import com.leaguesharing.service.service.ImageArticleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class ImageArticleController extends AbstractController  {

    private ImageArticleService imageArticleService;

    public ImageArticleController(ImageArticleService imageArticleService) {
        this.imageArticleService = imageArticleService;
    }

    @RequestMapping(value = "imagearticle/{uid}", method = RequestMethod.GET)
    public ImageArticleDTO getArticleByUid(@PathVariable String uid) {
        return imageArticleService.getImageArticleByUid(uid);
    }

    @RequestMapping(value = "imagearticles", method = RequestMethod.GET)
    public ArrayList<ImageArticleDTO> getImageArticles() {
        return imageArticleService.getImageArticles();
    }

    @RequestMapping(value = "imageArticle", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
    public ImageArticleDTO addArticle(@RequestBody ImageArticleDTO article) {
        return imageArticleService.addArticle(article, getContextDetails().toString());
    }

    @RequestMapping(value= "imagearticles/{uid}", method = RequestMethod.GET)
    public ArrayList<ImageArticleDTO> getArticlesByUserUid(@PathVariable String uid){
        System.out.println("LISTE Image ARTICLES:" + imageArticleService.getImageArticlesByArticleUid(uid));
        return imageArticleService.getImageArticlesByArticleUid(uid);
    }

    @RequestMapping(value = "imagearticle/delete/{uid}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONNAL')")
    public Long deleteByUid(@PathVariable String uid) {
        System.out.println(uid);
        return imageArticleService.deleteByUid(uid, getContextDetails().toString());
    }
}
