package com.leaguesharing.service.controller;

import java.util.ArrayList;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leaguesharing.service.model.dto.LikeUserDTO;
import com.leaguesharing.service.service.LikeUserService;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class LikeUserController extends AbstractController{

	private LikeUserService likeUserService;
	
	LikeUserController(LikeUserService likeUserService){
		this.likeUserService = likeUserService;
	}
	
	@RequestMapping(value = "likeUser/{uid}", method = RequestMethod.GET)
	public LikeUserDTO getLikeUserByUid(@PathVariable String uid) {
		return likeUserService.getLikeUserByUid(uid);
	}
	
	@RequestMapping(value = "likeUser/userLiked/{idUserLiked}/user/{idUser}", method = RequestMethod.GET)
	public LikeUserDTO getLikeUserByUserLikedUidAndUserUid(@PathVariable String idUserLiked, @PathVariable String idUser) {
		return likeUserService.getLikeUserByUserLikedUidAndUserUid(idUserLiked, idUser);
	}
	
	@RequestMapping(value = "LikeUser", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public LikeUserDTO addLikeUser(@RequestBody LikeUserDTO likeUser) {	
		return likeUserService.addLikeUser(likeUser, getContextDetails().toString());
	}
	
	@RequestMapping(value = "likeUser/delete/{uid}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public Long deleteByUid(@PathVariable String uid) {
		System.out.println(uid);
		return likeUserService.deleteByUid(uid, getContextDetails().toString());
	}
	
	@RequestMapping(value = "likeUser/userLiked/{userUid}", method = RequestMethod.GET)
	public ArrayList<LikeUserDTO> getLikeUserByUserLikedUid(@PathVariable String userUid) {
		return likeUserService.getLikeUserByUserLikedUid(userUid);
	}
}
