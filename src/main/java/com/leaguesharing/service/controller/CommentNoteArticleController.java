package com.leaguesharing.service.controller;

import java.util.ArrayList;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leaguesharing.service.model.dto.CommentNoteArticleDTO;
import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.dto.CommentNoteUserDTO;
import com.leaguesharing.service.model.dto.FavoryDTO;
import com.leaguesharing.service.service.CommentNoteArticleService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class CommentNoteArticleController extends AbstractController {
	
	private CommentNoteArticleService commentNoteArticleService;
	
	public CommentNoteArticleController(CommentNoteArticleService commentNoteArticleService) {
		this.commentNoteArticleService=commentNoteArticleService;
	}
	
	@RequestMapping(value = "commentNoteArticle/{uid}", method = RequestMethod.GET)
	public CommentNoteArticleDTO getCommentNoteArticleByUid(@PathVariable String uid) {
		return commentNoteArticleService.getCommentNoteArticleByUid(uid);
	}
	
	@RequestMapping(value= "commentNoteArticles", method =RequestMethod.GET)
	public ArrayList<CommentNoteArticleDTO> getCommentNoteArticles(){
		return commentNoteArticleService.getCommentNoteArticles();
	}
	
	@RequestMapping(value= "commentNoteArticle/article/{uid}", method=RequestMethod.GET)
	public ArrayList<CommentNoteArticleDTO> getCommentNoteArticleByRecipientUid(@PathVariable String uid){
		return commentNoteArticleService.getCommentNoteByArticleUid(uid);
	}
	
	@RequestMapping(value = "commentNoteArticle", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public CommentNoteArticleDTO addCommentNoteArticle(@RequestBody CommentNoteArticleDTO commentNoteArticle) {	
		return commentNoteArticleService.addCommentNoteArticle(commentNoteArticle, getContextDetails().toString());
	}
	
	@RequestMapping(value = "commentNoteArticle/delete/{uid}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public Long deleteByUid(@PathVariable String uid) {
		System.out.println(uid);
		return commentNoteArticleService.deleteByUid(uid, getContextDetails().toString());
	}
	
	 @RequestMapping(value = "commentNoteArticle/article/{uid}/writer/{idWriter}", method = RequestMethod.GET)
	 public ArrayList<CommentNoteArticleDTO> getCommentNoteArticleByArticleAndWriterUid(@PathVariable String uid, @PathVariable String idWriter){
		 return commentNoteArticleService.getCommentNoteArticleByArticleUidAndWriterUid(uid,idWriter);
	 }
	 
}
