package com.leaguesharing.service.controller;

import java.util.ArrayList;

import com.leaguesharing.service.model.adapter.CategoryAdapter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.leaguesharing.service.model.dto.*;
import com.leaguesharing.service.service.*;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class ArticleController extends AbstractController {
	
	private ArticleService articleService;
	
	public ArticleController(ArticleService articleService) {
		this.articleService= articleService;
	}
	
	@RequestMapping(value = "article/{uid}", method = RequestMethod.GET)
	public ArticleDTO getArticleByUid(@PathVariable String uid) {
		return articleService.getArticleByUid(uid);
	}
	
	@RequestMapping(value = "articles", method = RequestMethod.GET)
	public ArrayList<ArticleDTO> getArticles() {
		return articleService.getArticles();
	}
	
	@RequestMapping(value = "article", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public ArticleDTO addArticle(@RequestBody ArticleDTO article) {	
		return articleService.addArticle(article, getContextDetails().toString());
	}
	
	@RequestMapping(value= "articles/{uid}", method = RequestMethod.GET)
	public ArrayList<ArticleDTO> getArticlesByUserUid(@PathVariable String uid){
		System.out.println("LISTE ARTICLES:" + articleService.getArticlesByUserUid(uid));
		return articleService.getArticlesByUserUid(uid);
	}
	
	@RequestMapping(value= "articles/type/{type}", method = RequestMethod.GET)
	public ArrayList<ArticleDTO> getArticlesByType(@PathVariable String type){
		return articleService.getArticlesByType(type);
	}
	
	@RequestMapping(value = "article/delete/{uid}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONNAL')")
	public Long deleteByUid(@PathVariable String uid) {
		System.out.println(uid);
		return articleService.deleteByUid(uid, getContextDetails().toString());
	}
}
