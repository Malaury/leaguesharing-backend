package com.leaguesharing.service.controller;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractController {
	protected Object getContextDetails(){
		return SecurityContextHolder.getContext().getAuthentication().getDetails();
	}
	protected boolean haveAuthority(String auth) {
		for(GrantedAuthority a : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if(a.getAuthority().equalsIgnoreCase(auth)) {
				return true;
			}
		}
		
		return false;
	}
}
