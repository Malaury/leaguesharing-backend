package com.leaguesharing.service.controller;

import java.util.ArrayList;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leaguesharing.service.model.dto.CommentNoteArticleDTO;
import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.dto.CommentNoteUserDTO;
import com.leaguesharing.service.service.CommentNoteUserService;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class CommentNoteUserController extends AbstractController {
	
	private CommentNoteUserService commentNoteUserService;
	
	public CommentNoteUserController(CommentNoteUserService commentNoteUserService) {
		this.commentNoteUserService=commentNoteUserService;
	}
	
	@RequestMapping(value = "commentNoteUser/{uid}", method = RequestMethod.GET)
	public CommentNoteUserDTO getCommentNoteUserByUid(@PathVariable String uid) {
		return commentNoteUserService.getCommentNoteUserByUid(uid);
	}
	
	@RequestMapping(value= "commentNoteUsers", method =RequestMethod.GET)
	public ArrayList<CommentNoteUserDTO> getCommentNoteUsers(){
		return commentNoteUserService.getCommentNoteUsers();
	}
	
	@RequestMapping(value= "commentNoteUser/recipient/{uid}", method=RequestMethod.GET)
	public ArrayList<CommentNoteUserDTO> getCommentNoteUserByRecipientUid(@PathVariable String uid){
		return commentNoteUserService.getCommentNoteByRecipientUid(uid);
	}
	
	
	@RequestMapping(value= "commentNoteUser/user/{idUser}/recipient/{idRecipient}", method= RequestMethod.GET)
	public ArrayList<CommentNoteUserDTO> getgetCommentNoteUserByUserIdAndRecipientId(@PathVariable String idUser, @PathVariable String idRecipient){
		return commentNoteUserService.getCommentNoteUserByUserIdAndRecipientId(idUser,idRecipient);
	}
	
	@RequestMapping(value = "commentNoteUser", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public CommentNoteUserDTO addCommentNoteUser(@RequestBody CommentNoteUserDTO commentNoteUser) {	
		return commentNoteUserService.addCommentNoteUser(commentNoteUser, getContextDetails().toString());
	}
	
	@RequestMapping(value = "commentNoteUser/delete/{uid}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public Long deleteByUid(@PathVariable String uid) {
		System.out.println(uid);
		return commentNoteUserService.deleteByUid(uid, getContextDetails().toString());
	}
	
}
