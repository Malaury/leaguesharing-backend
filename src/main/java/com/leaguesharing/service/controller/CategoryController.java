package com.leaguesharing.service.controller;

import com.leaguesharing.service.model.dto.CategoryDTO;
import com.leaguesharing.service.service.CategoryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class CategoryController extends AbstractController{

    private CategoryService categoryService;

    private CategoryController(CategoryService categoryService) {
        this.categoryService= categoryService;
    }

    @RequestMapping(value = "category/{uid}", method = RequestMethod.GET)
    public CategoryDTO getCategoryByUid(@PathVariable String uid) {
        return  categoryService.getCategoryByUid(uid);
    }

    @RequestMapping(value = "categories", method = RequestMethod.GET)
    public ArrayList<CategoryDTO> getCategories() {
        return categoryService.getCategories();
    }

    @RequestMapping(value = "category", method = RequestMethod.POST)
    public CategoryDTO addCategory(@RequestBody CategoryDTO categoryDTO){
        return categoryService.addCategory(categoryDTO, getContextDetails().toString());
    }

    @RequestMapping(value = "categories/{uid}", method = RequestMethod.GET)
    public ArrayList<CategoryDTO> getCategoriesByParentUid(@PathVariable String uid){
        return categoryService.getCategoriesByParent(uid);
    }

    @RequestMapping(value = "categories/basic", method = RequestMethod.GET)
    public ArrayList<CategoryDTO> getBasicCategories(){
        System.out.println("ici");
        return categoryService.getBasicCategories();
    }

    @RequestMapping(value = "categories/children", method = RequestMethod.GET)
    public ArrayList<CategoryDTO> getCategoryChildren(){
        ArrayList<CategoryDTO> list = categoryService.getCategoryChildren();

        System.out.println(list);
        return list;
    }

    @RequestMapping(value = "category/delete/{uid}", method = RequestMethod.DELETE)
    public Long deleteByUid(@PathVariable String uid) {
        return categoryService.deleteByUid(uid, getContextDetails().toString());
    }
}
