package com.leaguesharing.service.controller;
import java.util.ArrayList;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.leaguesharing.service.model.dto.*;
import com.leaguesharing.service.service.*;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class UserController extends AbstractController {
	private UserService userService;
	
	public UserController(UserService userService) {
		this.userService=userService;
	}
	
	@RequestMapping(value = "user/{uid}", method = RequestMethod.GET)
	public UserDTO getUserByUid(@PathVariable String uid) {
		return userService.getUserByUid(uid);
	}
	
	@RequestMapping(value= "users", method =RequestMethod.GET)
	public ArrayList<UserDTO> getUsers(){
		return userService.getUsers();
	}
	
	@RequestMapping(value= "user/pseudo/{pseudo}", method=RequestMethod.GET)
	public UserDTO getUserByPseudo(@PathVariable String pseudo) {
		return userService.getUserByPseudo(pseudo);
	}
	
	@RequestMapping(value= "user/email/{email:.+}", method=RequestMethod.GET)
	public UserDTO getUserByEmail(@PathVariable String email) {
		return userService.getUserByEmail(email);
	}
	
	@RequestMapping(value = "user", method = RequestMethod.POST)
	public UserDTO addUser(@RequestBody UserDTO user) {
		return userService.addUser(user);
	}
	
	@RequestMapping(value = "user/update", method = RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public UserDTO updateUser(@RequestBody UserDTO user) {
		return userService.updateUser(user, getContextDetails().toString());
	}
	
	@RequestMapping(value = "user/update/password", method = RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public UserDTO updateUserPassword(@RequestBody UserDTO user) {
		return userService.updatePassword(user, getContextDetails().toString());
	}
	
}
