package com.leaguesharing.service.controller;

import java.util.ArrayList;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leaguesharing.service.model.dto.ArticleDTO;
import com.leaguesharing.service.model.dto.FavoryDTO;
import com.leaguesharing.service.model.entity.Article;
import com.leaguesharing.service.model.entity.User;
import com.leaguesharing.service.service.FavoryService;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(origins = "*")
public class FavoryController extends AbstractController {

	private FavoryService favoryService;
	
	public FavoryController(FavoryService favoryService) {
		this.favoryService=favoryService;
	}
	
	@RequestMapping(value = "favory/{uid}", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public FavoryDTO getFavoryByUid(@PathVariable String uid) {
		return favoryService.getFavoryByUid(uid);
	}
	
	@RequestMapping(value = "favories", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public ArrayList<FavoryDTO> getFavories() {
		return favoryService.getFavories();
	}
	
	@RequestMapping(value = "favory/article/{idArticle}/user/{idUser}", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONAL')")
	public FavoryDTO getFavoryByArticleAndUser(@PathVariable String idArticle, @PathVariable String idUser) {
		return favoryService.getFavoryByArticleAndUser(idArticle, idUser, getContextDetails().toString());
	}
	
	@RequestMapping(value = "favory", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONNAL')")
	public FavoryDTO addFavory(@RequestBody FavoryDTO favory) {	
		return favoryService.addFavory(favory, getContextDetails().toString());
	}
	
	@RequestMapping(value = "favory/delete/{uid}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONNAL')")
	public Long deleteByUid(@PathVariable String uid) {
		System.out.println(uid);
		return favoryService.deleteByUid(uid, getContextDetails().toString());
	}
	
	@RequestMapping(value = "favory/user/{userUid}", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('PARTICULAR', 'PROFESSIONNAL')")
	public ArrayList<FavoryDTO> getFavoryByUserUid(@PathVariable String userUid) {
		return favoryService.getFavoryByUserUid(userUid, getContextDetails().toString());
	}
}
