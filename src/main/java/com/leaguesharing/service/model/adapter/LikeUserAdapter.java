package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.LikeUserDTO;
import com.leaguesharing.service.model.entity.LikeUser;

public class LikeUserAdapter {
	public static LikeUserDTO toDTO(LikeUser f) {
		if(f==null) return null;
		
		LikeUserDTO dto = new LikeUserDTO();
		
		dto.setUid(f.getUid());
		dto.setUserLiked(UserAdapter.toDTO(f.getUserLiked()));
		dto.setUser(UserAdapter.toDTO(f.getUser()));
		
		return dto;
	}
	
	public static LikeUser toEntity(LikeUserDTO dto) {
		if(dto==null) return null;
		
		LikeUser f = new LikeUser();
		
		f.setUid(dto.getUid());
		f.setUserLiked(UserAdapter.toEntity(dto.getUserLiked()));
		f.setUser(UserAdapter.toEntity(dto.getUser()));
		
		return f;
	}
	
	public static ArrayList<LikeUserDTO> toDTO(Iterable<LikeUser> itLikeUser){
		ArrayList<LikeUserDTO> lLikeUser = new ArrayList<LikeUserDTO>();
		
		for(LikeUser r : itLikeUser) {
			lLikeUser.add(LikeUserAdapter.toDTO(r));
		}
		
		return lLikeUser;
	}
	public static ArrayList<LikeUser> toEntity(Iterable<LikeUserDTO> itLikeUser){
		ArrayList<LikeUser> lLikeUser = new ArrayList<LikeUser>();
		
		for(LikeUserDTO r : itLikeUser) {
			lLikeUser.add(LikeUserAdapter.toEntity(r));
		}
		
		return lLikeUser;
	}

}
