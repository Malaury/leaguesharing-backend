package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.ArticleDTO;
import com.leaguesharing.service.model.entity.Article;

public class ArticleAdapter {
	public static ArticleDTO toDTO(Article r) {
		if(r==null) return null;
		
		ArticleDTO dto = new ArticleDTO();
		
		dto.setUid(r.getUid());
		dto.setDescription(r.getDescription());
		dto.setTitle(r.getTitle());
		dto.setPrice(r.getPrice());
		dto.setUrlImage(r.getUrlImage());
		dto.setUser(UserAdapter.toDTO(r.getUser()));
		dto.setType(r.getType());
		dto.setIntention(r.getIntention());
		dto.setCategory(CategoryAdapter.toDTO(r.getCategory()));
		dto.setPlace(r.getPlace());
		dto.setLatitude(r.getLatitude());
		dto.setLongitude(r.getLongitude());
		
		return dto;
	}
	public static Article toEntity(ArticleDTO dto) {
		if(dto==null) return null;
		
		Article r = new Article();
		
		r.setUid(dto.getUid());
		r.setDescription(dto.getDescription());
		r.setTitle(dto.getTitle());
		r.setPrice(dto.getPrice());
		r.setUrlImage(dto.getUrlImage());
		r.setUser(UserAdapter.toEntity(dto.getUser()));
		r.setType(dto.getType());
		r.setIntention(dto.getIntention());
		r.setCategory(r.getCategory());
		r.setPlace(dto.getPlace());
		r.setLatitude(dto.getLatitude());
		r.setLongitude(dto.getLongitude());
		
		return r;
	}
	public static ArrayList<ArticleDTO> toDTO(Iterable<Article> itArticle){
		ArrayList<ArticleDTO> lArticle = new ArrayList<ArticleDTO>();
		
		for(Article r : itArticle) {
			lArticle.add(ArticleAdapter.toDTO(r));
		}
		
		return lArticle;
	}
	public static ArrayList<Article> toEntity(Iterable<ArticleDTO> itArticle){
		ArrayList<Article> lArticle = new ArrayList<Article>();
		
		for(ArticleDTO r : itArticle) {
			lArticle.add(ArticleAdapter.toEntity(r));
		}
		
		return lArticle;
	}
}
