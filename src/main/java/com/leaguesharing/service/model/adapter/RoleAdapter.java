package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.RoleDTO;
import com.leaguesharing.service.model.entity.Role;

public class RoleAdapter {
	public static RoleDTO toDTO(Role r) {
		if(r==null) return null;
		
		RoleDTO dto = new RoleDTO();
		
		dto.setUid(r.getUid());
		dto.setName(r.getName());
		
		return dto;
	}
	public static Role toEntity(RoleDTO dto) {
		if(dto==null) return null;
		
		Role r = new Role();
		
		r.setUid(dto.getUid());
		r.setName(dto.getName());
		
		return r;
	}
	public static ArrayList<RoleDTO> toDTO(Iterable<Role> itRole){
		ArrayList<RoleDTO> lRole = new ArrayList<RoleDTO>();
		
		for(Role r : itRole) {
			lRole.add(RoleAdapter.toDTO(r));
		}
		
		return lRole;
	}
	public static ArrayList<Role> toEntity(Iterable<RoleDTO> itRole){
		ArrayList<Role> lRole = new ArrayList<Role>();
		
		for(RoleDTO r : itRole) {
			lRole.add(RoleAdapter.toEntity(r));
		}
		
		return lRole;
	}
}
