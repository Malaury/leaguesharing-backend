package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.UserDTO;
import com.leaguesharing.service.model.entity.User;

public class UserAdapter {
	public static UserDTO toDTO(User u) {
		if(u==null) return null;
		
		UserDTO dto = new UserDTO();
		dto.setCreateDate(u.getCreatDate());
		dto.setModDate(u.getModDate());
		dto.setUid(u.getUid());
		dto.setEmail(u.getEmail());
		dto.setRole(RoleAdapter.toDTO(u.getRole()));
		dto.setGender(u.getGender());
		dto.setPseudo(u.getPseudo());
		dto.setDescription(u.getDescription());
		dto.setUrlImageProfil(u.getUrlImageProfil());
		
		return dto;
	}
	public static User toEntity(UserDTO dto) {
		if(dto==null) return null;
		
		User u = new User();
		
		u.setCreatDate(dto.getCreateDate());
		u.setModDate(dto.getModDate());
		u.setUid(dto.getUid());
		u.setEmail(dto.getEmail());
		u.setRole(RoleAdapter.toEntity(dto.getRole()));
		u.setGender(dto.getGender());
		u.setPseudo(dto.getPseudo());
		u.setPassword(dto.getPassword());
		u.setDescription(dto.getDescription());
		u.setUrlImageProfil(dto.getUrlImageProfil());
		
		return u;
	}
	public static ArrayList<UserDTO> toDTO(Iterable<User> itUser){
		ArrayList<UserDTO> lUser = new ArrayList<UserDTO>();
		
		for(User u : itUser) {
			lUser.add(UserAdapter.toDTO(u));
		}
		
		return lUser;
	}
	public static ArrayList<User> toEntity(Iterable<UserDTO> itUser){
		ArrayList<User> lUser = new ArrayList<User>();
		
		for(UserDTO u : itUser) {
			lUser.add(UserAdapter.toEntity(u));
		}
		
		return lUser;
	}
}
