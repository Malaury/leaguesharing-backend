package com.leaguesharing.service.model.adapter;

import com.leaguesharing.service.model.dto.TchatDTO;
import com.leaguesharing.service.model.entity.Tchat;

import java.util.ArrayList;

public class TchatAdapter {
    public static TchatDTO toDTO(Tchat r) {
        if(r==null) return null;

        TchatDTO dto = new TchatDTO();

        dto.setUid(r.getUid());
        dto.setUsers(UserAdapter.toDTO(r.getUsers()));

        return dto;
    }
    public static Tchat toEntity(TchatDTO dto) {
        if(dto==null) return null;

        Tchat r = new Tchat();

        r.setUid(dto.getUid());
        r.setUsers(UserAdapter.toEntity(dto.getUsers()));

        return r;
    }
    public static ArrayList<TchatDTO> toDTO(Iterable<Tchat> itTchat){
        ArrayList<TchatDTO> lTchat = new ArrayList<TchatDTO>();

        for(Tchat r : itTchat) {
            lTchat.add(TchatAdapter.toDTO(r));
        }

        return lTchat;
    }
    public static ArrayList<Tchat> toEntity(Iterable<TchatDTO> itTchat){
        ArrayList<Tchat> lTchat = new ArrayList<Tchat>();

        for(TchatDTO r : itTchat) {
            lTchat.add(TchatAdapter.toEntity(r));
        }

        return lTchat;
    }
}
