package com.leaguesharing.service.model.adapter;

import com.leaguesharing.service.model.dto.CategoryDTO;
import com.leaguesharing.service.model.entity.Category;

import java.util.ArrayList;

public class CategoryAdapter {
    public static CategoryDTO toDTO(Category c){
        if(c == null) return null;

        CategoryDTO dto = new CategoryDTO();

        System.out.println(c);

        dto.setUid(c.getUid());
        dto.setCategoryName(c.getCategoryName());
        dto.setCategoryParent(c.getCategoryParent());

        System.out.println(dto.toString());

        return dto;
    }

    public static Category toEntity(CategoryDTO dto){
        if(dto==null) return null;

        Category c = new Category();

        c.setCategoryName(dto.getCategoryName());
        c.setCategoryParent(dto.getCategoryParent());
        c.setUid(dto.getUid());

        return c;
    }

    public static ArrayList<CategoryDTO> toDTO(Iterable<Category> itCategory){
        ArrayList<CategoryDTO> lCategory = new ArrayList<>();

        for(Category r : itCategory) {
            lCategory.add(CategoryAdapter.toDTO(r));
        }

        return lCategory;
    }
    public static ArrayList<Category> toEntity(Iterable<CategoryDTO> itCategory){
        ArrayList<Category> lCategory = new ArrayList<>();

        for(CategoryDTO r : itCategory) {
            lCategory.add(CategoryAdapter.toEntity(r));
        }

        return lCategory;
    }
}
