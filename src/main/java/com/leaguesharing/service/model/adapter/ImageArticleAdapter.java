package com.leaguesharing.service.model.adapter;

import com.leaguesharing.service.model.dto.ImageArticleDTO;
import com.leaguesharing.service.model.entity.ImageArticle;

import java.util.ArrayList;

public class ImageArticleAdapter {

    public static ImageArticleDTO toDTO(ImageArticle imageArticle){

        if(imageArticle==null) return null;
        ImageArticleDTO dto = new ImageArticleDTO();

        dto.setUid(imageArticle.getUid());
        dto.setUrl(imageArticle.getUrl());
        dto.setArticle(ArticleAdapter.toDTO(imageArticle.getArticle()));

        return dto;
    }

    public static ImageArticle toEntity(ImageArticleDTO dto){
        if(dto==null)return null;

        ImageArticle imageArticle = new ImageArticle();

        imageArticle.setUid(dto.getUid());
        imageArticle.setUrl(dto.getUrl());
        imageArticle.setArticle(ArticleAdapter.toEntity(dto.getArticle()));

        return imageArticle;
    }

    public static ArrayList<ImageArticleDTO> toDTO(Iterable<ImageArticle> itArticle){
        ArrayList<ImageArticleDTO> lImageArticle = new ArrayList<ImageArticleDTO>();

        for(ImageArticle r : itArticle) {
            lImageArticle.add(ImageArticleAdapter.toDTO(r));
        }

        return lImageArticle;
    }
    public static ArrayList<ImageArticle> toEntity(Iterable<ImageArticleDTO> itArticle){
        ArrayList<ImageArticle> lImageArticle = new ArrayList<ImageArticle>();

        for(ImageArticleDTO r : itArticle) {
            lImageArticle.add(ImageArticleAdapter.toEntity(r));
        }

        return lImageArticle;
    }
}
