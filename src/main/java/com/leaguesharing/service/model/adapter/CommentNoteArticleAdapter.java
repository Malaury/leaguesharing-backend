package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.CommentNoteArticleDTO;
import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.entity.CommentNote;
import com.leaguesharing.service.model.entity.CommentNoteArticle;

public class CommentNoteArticleAdapter {
	
	public static CommentNoteArticleDTO toDTO(CommentNoteArticle c) {
		if(c==null) return null;
		
		CommentNoteArticleDTO dto = new CommentNoteArticleDTO();
		
		dto.setUid(c.getUid());
		dto.setWriter(UserAdapter.toDTO(c.getWriter()));
		dto.setArticle(ArticleAdapter.toDTO(c.getArticle()));
		dto.setComment(c.getComment());
		dto.setNote(c.getNote());
		
		return dto;
	}
	
	public static CommentNoteArticle toEntity(CommentNoteArticleDTO dto) {
		if(dto==null) return null;
		CommentNoteArticle c = new CommentNoteArticle();
		
		c.setUid(dto.getUid());
		c.setArticle(ArticleAdapter.toEntity(dto.getArticle()));
		c.setWriter(UserAdapter.toEntity(dto.getWriter()));
		c.setComment(dto.getComment());
		c.setNote(dto.getNote());
		
		return c;
	}
	
	public static ArrayList<CommentNoteArticleDTO> toDTO(Iterable<CommentNoteArticle> itCommentNoteArticle){
		ArrayList<CommentNoteArticleDTO> lCommentNoteArticle = new ArrayList<CommentNoteArticleDTO>();
		
		for(CommentNoteArticle r : itCommentNoteArticle) {
			lCommentNoteArticle.add(CommentNoteArticleAdapter.toDTO(r));
		}
		
		return lCommentNoteArticle;
	}
	public static ArrayList<CommentNoteArticle> toEntity(Iterable<CommentNoteArticleDTO> itCommentNoteArticle){
		ArrayList<CommentNoteArticle> lCommentNoteArticle = new ArrayList<CommentNoteArticle>();
		
		for(CommentNoteArticleDTO r : itCommentNoteArticle) {
			lCommentNoteArticle.add(CommentNoteArticleAdapter.toEntity(r));
		}
		
		return lCommentNoteArticle;
	}
}
