package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.ArticleDTO;
import com.leaguesharing.service.model.dto.FavoryDTO;
import com.leaguesharing.service.model.entity.Article;
import com.leaguesharing.service.model.entity.Favory;

public class FavoryAdapter {
	public static FavoryDTO toDTO(Favory f) {
		if(f==null) return null;
		
		FavoryDTO dto = new FavoryDTO();
		
		dto.setUid(f.getUid());
		dto.setArticle(ArticleAdapter.toDTO(f.getArticle()));
		dto.setUser(UserAdapter.toDTO(f.getUser()));
		
		return dto;
	}
	
	public static Favory toEntity(FavoryDTO dto) {
		if(dto==null) return null;
		
		Favory f = new Favory();
		
		f.setUid(dto.getUid());
		f.setArticle(ArticleAdapter.toEntity(dto.getArticle()));
		f.setUser(UserAdapter.toEntity(dto.getUser()));
		
		return f;
	}
	
	public static ArrayList<FavoryDTO> toDTO(Iterable<Favory> itFavory){
		ArrayList<FavoryDTO> lFavory = new ArrayList<FavoryDTO>();
		
		for(Favory r : itFavory) {
			lFavory.add(FavoryAdapter.toDTO(r));
		}
		
		return lFavory;
	}
	public static ArrayList<Favory> toEntity(Iterable<FavoryDTO> itFavory){
		ArrayList<Favory> lFavory = new ArrayList<Favory>();
		
		for(FavoryDTO r : itFavory) {
			lFavory.add(FavoryAdapter.toEntity(r));
		}
		
		return lFavory;
	}
}
