package com.leaguesharing.service.model.adapter;

import com.leaguesharing.service.model.dto.MessageDTO;
import com.leaguesharing.service.model.entity.Message;

import java.util.ArrayList;

public class MessageAdapter {
    public static MessageDTO toDTO(Message r) {
        if(r==null) return null;

        MessageDTO dto = new MessageDTO();

        dto.setUid(r.getUid());
        dto.setWriter(UserAdapter.toDTO(r.getWriter()));
        dto.setMessage(r.getMessage());

        return dto;
    }
    public static Message toEntity(MessageDTO dto) {
        if(dto==null) return null;

        Message r = new Message();

        r.setUid(dto.getUid());
        r.setWriter(UserAdapter.toEntity(dto.getWriter()));
        r.setMessage(dto.getMessage());

        return r;
    }
    public static ArrayList<MessageDTO> toDTO(Iterable<Message> itMessage){
        ArrayList<MessageDTO> lMessage = new ArrayList<MessageDTO>();

        for(Message r : itMessage) {
            lMessage.add(MessageAdapter.toDTO(r));
        }

        return lMessage;
    }
    public static ArrayList<Message> toEntity(Iterable<MessageDTO> itMessage){
        ArrayList<Message> lMessage = new ArrayList<Message>();

        for(MessageDTO r : itMessage) {
            lMessage.add(MessageAdapter.toEntity(r));
        }

        return lMessage;
    }
}
