package com.leaguesharing.service.model.adapter;

import java.util.ArrayList;

import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.dto.CommentNoteUserDTO;
import com.leaguesharing.service.model.entity.CommentNote;
import com.leaguesharing.service.model.entity.CommentNoteUser;

public class CommentNoteUserAdapter {

	public static CommentNoteUserDTO toDTO(CommentNoteUser c) {
		if(c==null) return null;
		
		CommentNoteUserDTO dto = new CommentNoteUserDTO();
		
		dto.setUid(c.getUid());
		dto.setWriter(UserAdapter.toDTO(c.getWriter()));
		dto.setRecipient(UserAdapter.toDTO(c.getRecipient()));
		dto.setNote(c.getNote());
		dto.setComment(c.getComment());
		return dto;
	}
	
	public static CommentNoteUser toEntity(CommentNoteUserDTO dto) {
		if(dto==null) return null;
		CommentNoteUser c = new CommentNoteUser();
		
		c.setUid(dto.getUid());
		c.setWriter(UserAdapter.toEntity(dto.getWriter()));
		c.setRecipient(UserAdapter.toEntity(dto.getRecipient()));
		c.setNote(dto.getNote());
		c.setComment(dto.getComment());
		
		return c;
	}
	
	public static ArrayList<CommentNoteUserDTO> toDTO(Iterable<CommentNoteUser> itCommentNoteUser){
		ArrayList<CommentNoteUserDTO> lCommentNoteUser = new ArrayList<CommentNoteUserDTO>();
		
		for(CommentNoteUser r : itCommentNoteUser) {
			lCommentNoteUser.add(CommentNoteUserAdapter.toDTO(r));
		}
		
		return lCommentNoteUser;
	}
	public static ArrayList<CommentNoteUser> toEntity(Iterable<CommentNoteUserDTO> itCommentNoteUser){
		ArrayList<CommentNoteUser> lCommentNoteUser = new ArrayList<CommentNoteUser>();
		
		for(CommentNoteUserDTO r : itCommentNoteUser) {
			lCommentNoteUser.add(CommentNoteUserAdapter.toEntity(r));
		}
		
		return lCommentNoteUser;
	}
}
