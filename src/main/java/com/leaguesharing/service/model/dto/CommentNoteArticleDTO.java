package com.leaguesharing.service.model.dto;

public class CommentNoteArticleDTO extends CommentNoteDTO {

	private ArticleDTO article;
	
	public CommentNoteArticleDTO(UserDTO writer,String comment, int note, ArticleDTO article) {
		super();
		this.article=article;
	}

	public CommentNoteArticleDTO() {
		
	}
	
	public ArticleDTO getArticle() {
		return article;
	}

	public void setArticle(ArticleDTO article) {
		this.article = article;
	}
}
