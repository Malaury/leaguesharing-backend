package com.leaguesharing.service.model.dto;

import com.leaguesharing.service.model.entity.Message;
import com.leaguesharing.service.model.entity.Tchat;

import java.util.List;

public class TchatDTO extends BaseDTO {

    private List<UserDTO> users;

    private List<MessageDTO> messages;

    public TchatDTO(List<UserDTO> users, List<MessageDTO> messages){
        this.users=users;
        this.messages=messages;
    }

    public TchatDTO (){

    }

    public String toString() {
        return "TchatDTO{ \n users: "
                + users.toString() + "\n, messages: "
                + messages.toString() +"};";
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }
}
