package com.leaguesharing.service.model.dto;

import com.leaguesharing.service.model.entity.ImageArticle;

public class ImageArticleDTO extends BaseDTO {

    private String url;

    private ArticleDTO article;

    public ImageArticleDTO(String url, ArticleDTO article){
        this.url=url;
        this.article=article;
    }

    public ImageArticleDTO(){
    }

    @Override
    public String toString() {
        return "ImageArticleDTO{" +
                "url='" + url+ '\'' +
                ", article=" + article +
                '}';
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArticleDTO getArticle() {
        return article;
    }

    public void setArticle(ArticleDTO article) {
        this.article = article;
    }
}
