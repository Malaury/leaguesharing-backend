package com.leaguesharing.service.model.dto;

import com.leaguesharing.service.model.entity.User;

public class CommentNoteDTO extends BaseDTO {

	private UserDTO writer;
	private String comment;
	private int note;
	
	public CommentNoteDTO() {
		
	}
	
	public CommentNoteDTO(UserDTO writer,String comment, int note) {
		this.writer=writer;
		this.comment= comment;
		this.note=note;
	}

	public UserDTO getWriter() {
		return writer;
	}

	public void setWriter(UserDTO writer) {
		this.writer = writer;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}
}
