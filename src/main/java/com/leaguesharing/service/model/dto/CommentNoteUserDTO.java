package com.leaguesharing.service.model.dto;

public class CommentNoteUserDTO extends CommentNoteDTO{

	private UserDTO recipient;
	
	public CommentNoteUserDTO(UserDTO writer,String comment, int note, UserDTO recipient) {
		super();
		this.recipient=recipient;
	}
	
	public CommentNoteUserDTO() {
		
	}

	public UserDTO getRecipient() {
		return recipient;
	}

	public void setRecipient(UserDTO recipient) {
		this.recipient = recipient;
	}
	
	
	
}
