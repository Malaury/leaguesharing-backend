package com.leaguesharing.service.model.dto;

import com.leaguesharing.service.model.entity.Category;

public class CategoryDTO extends BaseDTO{

    private String categoryName;
    private Category categoryParent;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Category getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(Category categoryParent) {
        this.categoryParent = categoryParent;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "categoryName='" + categoryName + '\'' +
                ", categoryParent=" + categoryParent +
                '}';
    }
}
