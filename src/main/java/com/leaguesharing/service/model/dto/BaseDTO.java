package com.leaguesharing.service.model.dto;

public abstract class BaseDTO {
	/**
	 * Member variables
	 *********************************************************************/
	
	private String uid;

	/**
	 * End of Member variables
	 **************************************************************/

	/**
	 * Constructor
	 **************************************************************************/
	
	public BaseDTO() {}

	/**
	 * End of constructor
	 *******************************************************************/

	/**
	 * getters / setters
	 ********************************************************************/

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	/**
	 * End of getters / setters
	 *************************************************************/
}