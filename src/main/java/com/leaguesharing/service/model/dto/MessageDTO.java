package com.leaguesharing.service.model.dto;

import com.leaguesharing.service.model.entity.Message;

import java.time.LocalDateTime;

public class MessageDTO extends BaseDTO {

    private LocalDateTime date;

    private UserDTO writer;

    private String message;

    public MessageDTO(LocalDateTime date, UserDTO writer, String message){
        this.date=date;
        this.writer=writer;
        this.message=message;
    }

    public MessageDTO(){

    }

    @Override
    public String toString(){
        return "MessageDTO" +"\n" +
                "date=" + date + "+\n" +
                ",writer=" + writer.toString() + "\n" +
                ",message=" + message + "\n" +
                "};";
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public UserDTO getWriter() {
        return writer;
    }

    public void setWriter(UserDTO writer) {
        this.writer = writer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
