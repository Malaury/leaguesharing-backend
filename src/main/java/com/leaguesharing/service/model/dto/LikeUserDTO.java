package com.leaguesharing.service.model.dto;

public class LikeUserDTO extends BaseDTO {
	
	private UserDTO user;
	private UserDTO userLiked;
	
	public LikeUserDTO(UserDTO user, UserDTO userLiked) {
		super();
		this.user=user;
		this.userLiked=userLiked;
	}
	
	public LikeUserDTO() {
		
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public UserDTO getUserLiked() {
		return userLiked;
	}

	public void setUserLiked(UserDTO userLiked) {
		this.userLiked = userLiked;
	}
}
