package com.leaguesharing.service.model.dto;

public class RoleDTO extends BaseDTO {
	/**
	 * Member variables
	 *********************************************************************/

	private String name;

	/**
	 * End of Member variables
	 **************************************************************/
	
	/**
	 * Constructor
	 **************************************************************************/
	
	public RoleDTO() {
		super();
	}
	
	public RoleDTO(String name) {
		super();
		this.name = name;
	}

	/**
	 * End of constructor
	 *******************************************************************/
	
	/**
	 * getters / setters
	 ********************************************************************/
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * End of getters / setters
	 *************************************************************/
}
