package com.leaguesharing.service.model.dto;


public class FavoryDTO extends BaseDTO {
	private UserDTO user;
	private ArticleDTO article;
	
	public FavoryDTO(UserDTO user, ArticleDTO article) {
		this.user=user;
		this.article=article;
	}
	
	public FavoryDTO() {
		
	}
	
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public ArticleDTO getArticle() {
		return article;
	}
	public void setArticle(ArticleDTO article) {
		this.article = article;
	}
	
	
}
