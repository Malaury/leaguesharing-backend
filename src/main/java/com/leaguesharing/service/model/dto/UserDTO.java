package com.leaguesharing.service.model.dto;

import java.util.Date;

public class UserDTO extends BaseDTO {
	/**
	 * Member variables
	 *********************************************************************/

	private RoleDTO role;
	private String email;
	private String pseudo;
	private String gender;
	private String password;
	private String description;
	private Date createDate;
	private Date modDate;
	private String urlImageProfil;

	/**
	 * End of Member variables
	 **************************************************************/
	
	/**
	 * Constructor
	 **************************************************************************/
	
	public UserDTO() {
		super();
	}
	
	public UserDTO(RoleDTO role, String email, String pseudo, String gender, String description, String urlImageProfil) {
		super();
		this.role = role;
		this.email = email;
		this.pseudo=pseudo;
		this.gender=gender;
		this.description=description;
		this.urlImageProfil=urlImageProfil;
	}

	/**
	 * End of constructor
	 *******************************************************************/
	
	/**
	 * getters / setters
	 ********************************************************************/
	
	public RoleDTO getRole() {
		return role;
	}

	public void setRole(RoleDTO role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModDate() {
		return modDate;
	}

	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

	public String getUrlImageProfil() {
		return urlImageProfil;
	}

	public void setUrlImageProfil(String urlImageProfil) {
		this.urlImageProfil = urlImageProfil;
	}

	/**
	 * End of getters / setters
	 *************************************************************/
}