package com.leaguesharing.service.model.dao;

import com.leaguesharing.service.model.entity.Category;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CategoryDAO extends CrudRepository<Category, Long> {
    public Category findByUid(String uid);
    public Category save(Category category);
    public Iterable<Category> findByCategoryParentUid(String uid);
    public Iterable<Category> findByCategoryParentIsNull();

    public Iterable<Category> findByCategoryParentIsNotNull();
    @Modifying
    @Transactional
    public Long deleteByUid(String uid);
}
