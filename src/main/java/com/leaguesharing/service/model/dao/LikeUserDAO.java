package com.leaguesharing.service.model.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguesharing.service.model.entity.Favory;
import com.leaguesharing.service.model.entity.LikeUser;

public interface LikeUserDAO extends CrudRepository<LikeUser, Long> {
	
	public LikeUser findByUid(String uid);
	
	@Modifying
	@Transactional
	public Long deleteByUid(String uid);
	
	public Iterable<LikeUser> findByUserLikedUid(String uid);
	public LikeUser findByUserLikedUidAndUserUid(String idUserLiked, String idUser);
	
}
