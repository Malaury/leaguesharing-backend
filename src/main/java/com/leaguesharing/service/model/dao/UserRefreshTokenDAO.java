package com.leaguesharing.service.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.leaguesharing.service.model.entity.UserRefreshToken;

public interface UserRefreshTokenDAO extends CrudRepository<UserRefreshToken, Long> {
	UserRefreshToken findByTokenUid(String tokenuid);
	UserRefreshToken findByEmail(String email);
	UserRefreshToken findByEmailAndTokenUid(String email, String tokenUid);
}