package com.leaguesharing.service.model.dao;
import com.leaguesharing.service.model.dto.FavoryDTO;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.leaguesharing.service.model.entity.Article;
import com.leaguesharing.service.model.entity.Favory;

public interface FavoryDAO extends CrudRepository<Favory, Long> {
	public Favory findByUid(String uid);
	
	@Modifying
	@Transactional
	public Long deleteByUid(String uid);
	
	public Favory findByUserUidAndArticleUid(String idUser, String idArticle);
	
	public Iterable<Favory> findByUserUid(String uid);
}
