package com.leaguesharing.service.model.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguesharing.service.model.entity.CommentNote;
import com.leaguesharing.service.model.entity.CommentNoteArticle;
import com.leaguesharing.service.model.entity.CommentNoteUser;
import com.leaguesharing.service.model.entity.User;

public interface CommentNoteUserDAO extends CrudRepository<CommentNoteUser, Long> {
	
	public Iterable<CommentNoteUser> findByRecipientUid(String uid);
	public CommentNoteUser findByUid(String uid);
	@Modifying
	@Transactional
	public Long deleteByUid(String uid);
	public Iterable<CommentNoteUser> findByWriterUidAndRecipientUid(String idWriter, String idRecipient);
}
