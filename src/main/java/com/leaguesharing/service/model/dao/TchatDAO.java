package com.leaguesharing.service.model.dao;

import com.leaguesharing.service.model.entity.Tchat;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface TchatDAO extends CrudRepository<Tchat, Long> {

    public Tchat findByUid(String uid);
    public Tchat save(Tchat tchat);
    //public Iterable<Tchat> findByUserUid(String uid);

    @Modifying
    @Transactional
    public Long deleteByUid(String uid);
}
