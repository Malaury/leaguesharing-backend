package com.leaguesharing.service.model.dao;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguesharing.service.model.entity.CommentNoteArticle;

public interface CommentNoteArticleDAO  extends CrudRepository<CommentNoteArticle, Long> {
	
	public Iterable<CommentNoteArticle> findByArticleUid(String uid);
	public CommentNoteArticle findByUid(String uid);
	//public Iterable<CommentNoteArticle> findTop10ArticleUid(String uid, Pageable pageable);
	@Modifying
	@Transactional
	public Long deleteByUid(String uid);
	public Iterable<CommentNoteArticle> findByArticleUidAndWriterUid(String idArticle, String idWrtiter);
}
