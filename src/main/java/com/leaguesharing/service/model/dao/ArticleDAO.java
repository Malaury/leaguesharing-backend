package com.leaguesharing.service.model.dao;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguesharing.service.model.entity.*;

public interface ArticleDAO extends CrudRepository<Article, Long> {
	public Article findByUid(String uid);
	public Article save(Article article);
	public Iterable<Article> findByUserUid(String uid);
	public Iterable<Article> findByType(String uid);
	@Modifying
	@Transactional
	public Long deleteByUid(String uid);
}
