package com.leaguesharing.service.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.leaguesharing.service.model.entity.User;

public interface UserDAO extends CrudRepository<User, Long>{
	public User findByUid(String uid);
	public User findByPseudo(String pseudo);
	public User findByEmail(String email);
}
