package com.leaguesharing.service.model.dao;
import org.springframework.data.repository.CrudRepository;
import com.leaguesharing.service.model.entity.*;

public interface RoleDAO  extends CrudRepository<Role, Long> {
	public Role findByUid(String uid);
	public Role findByName(String name);
	public Iterable<Role> findAll();
}
