package com.leaguesharing.service.model.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "uid")
public abstract class BaseEntity {
	/**
	 * Member variables
	 *********************************************************************/
	
	/*
	 * General attributes
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(unique = true, updatable = false, length = 36)
	private String uid;

	@Version
	private Long version;

	/*
	 * Auditing attributes
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date creatDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modDate;

	/**
	 * End of Member variables
	 **************************************************************/

	/**
	 * Method
	 *******************************************************************************/
	
	@PrePersist
	public void prePersist() {
		Date now = new Date();
		setCreatDate(now);
		setModDate(now);
		if (getUid() == null || getUid().isEmpty()) {
			setUid(UUID.randomUUID().toString());
		}
	}

	@PreUpdate
	public void preUpdate() {
		Date now = new Date();
		setModDate(now);
	}

	/**
	 * End of Method
	 ************************************************************************/

	/**
	 * Constructor
	 **************************************************************************/
	
	public BaseEntity() {}

	/**
	 * End of constructor
	 *******************************************************************/

	/**
	 * getters / setters
	 ********************************************************************/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getCreatDate() {
		return creatDate;
	}

	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	public Date getModDate() {
		return modDate;
	}

	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
	
	/**
	 * End of getters / setters
	 *************************************************************/
}