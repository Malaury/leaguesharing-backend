package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Favory extends BaseEntity{
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="USER_ID", nullable=false)
	private User user;
	
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="ARTICLE_ID", nullable=false)
	private Article article;
	
	public Favory() {
		
	}
	
	public Favory(User user, Article article) {
		this.user=user;
		this.article=article;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
