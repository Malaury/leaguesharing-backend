package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LikeUser extends BaseEntity {
	
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="USER_ID", nullable=false)
	private User user;
	
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="USERLIKED_ID", nullable=false)
	private User userLiked;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUserLiked() {
		return userLiked;
	}

	public void setUserLiked(User userLiked) {
		this.userLiked = userLiked;
	}
	
	public LikeUser(User user, User userLiked) {
		this.user=user;
		this.userLiked=userLiked;
	}
	
	public LikeUser() {
		
	}
}
