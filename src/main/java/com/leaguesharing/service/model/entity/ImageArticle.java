package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ImageArticle extends BaseEntity{

    private String url;

    @ManyToOne(cascade={CascadeType.PERSIST})
    @JoinColumn(name="ARTICLE_ID", nullable=false)
    private Article article;

    public ImageArticle(String url , Article article){
        this.url=url;
        this.article=article;
    }

    public ImageArticle(){

    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
