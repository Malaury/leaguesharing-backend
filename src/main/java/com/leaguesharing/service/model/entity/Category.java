package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Category extends BaseEntity {
    @ManyToOne(cascade={CascadeType.PERSIST})
    @JoinColumn(name="PARENT_ID", nullable=true)
    private Category categoryParent;

    private String categoryName;


    public Category(Category categoryParent, String categoryName){
        this.categoryParent=categoryParent;
        this.categoryName=categoryName;
    }

    public Category(){

    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Category getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(Category categoryParent) {
        categoryParent = categoryParent;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryParent=" + categoryParent +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
