package com.leaguesharing.service.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tchat extends BaseEntity{

    @ManyToMany(cascade={CascadeType.PERSIST})
    @JoinColumn(name="TCHAT_USERS", nullable=false)
    private List<User> users;

    public Tchat(List<User> users){
        this.users=users;
    }

    public Tchat(){

    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
