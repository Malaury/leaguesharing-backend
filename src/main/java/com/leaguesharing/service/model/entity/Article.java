package com.leaguesharing.service.model.entity;

import javax.persistence.*;

@Entity
public class Article extends BaseEntity {
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="OWNER_ID", nullable=false)
	private User user;
	
	private String title;
	private String urlImage;
	private String description;
	private String price;
	private String type;
	private String intention;
	private double latitude;
	private double longitude;

	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST})
	@JoinColumn(name="CATEGORY_ID", nullable=false)
	private Category category;

	private String place;

	public Article(User user, String title, String urlImage, String description, String price, String type, String but, Category category, String lieu, double lat, double longitude){
		this.user=user;
		this.title=title;
		this.urlImage=urlImage;
		this.description=description;
		this.price=price;
		this.type=type;
		this.intention=intention;
		this.category=category;
		this.place=place;
		this.latitude=lat;
		this.longitude= longitude;
	}
	
	public Article(){
		
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIntention() {
		return intention;
	}

	public void setIntention(String intention) {
		this.intention = intention;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double lat) {
		this.latitude = lat;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
