package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CommentNoteUser extends CommentNote {

	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="USER_ID_RECIPIENT", nullable=false)
	private User recipient;
	
	public CommentNoteUser(User writer, String comment, int note, User recipient) {
		super();
		this.recipient=recipient;
	}
	
	public CommentNoteUser() {
		
	}

	public User getRecipient() {
		return recipient;
	}

	public void setRecipient(User recipient) {
		this.recipient = recipient;
	}
}
