package com.leaguesharing.service.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class UserRefreshToken extends BaseEntity {
	/**
	 * Member variables
	 *********************************************************************/
	
	@Column(nullable = false, unique = true)
	private String email;
	@Column(nullable = false, unique = true)
	private String tokenUid;
	private Boolean enable = true;

	/**
	 * End of Member variables
	 **************************************************************/

	/**
	 * Constructor
	 **************************************************************************/
	
	public UserRefreshToken() {
		super();
	}
	
	public UserRefreshToken(String email, String tokenUid) {
		super();
		this.setEmail(email);
		this.setTokenUid(tokenUid);
	}
	
	/**
	 * End of constructor
	 *******************************************************************/

	/**
	 * getters / setters
	 ********************************************************************/

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTokenUid() {
		return tokenUid;
	}

	public void setTokenUid(String tokenUid) {
		this.tokenUid = tokenUid;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	/**
	 * End of getters / setters
	 *************************************************************/
}
