package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class CommentNote extends BaseEntity {
	
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="USER_ID_WRITER", nullable=false)
	private User writer;
	
	@Column(nullable=false, unique=false, columnDefinition = "text")
	private String comment;
	
	@Column(nullable=false, unique=false)
	private int note;
	
	public CommentNote(){
	}
	
	public CommentNote(User writer, String comment, int note){
		this.writer=writer;
		this.comment=comment;
		this.note=note;
	}

	public User getWriter() {
		return writer;
	}

	public void setWriter(User writer) {
		this.writer = writer;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}
}
