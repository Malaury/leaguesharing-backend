package com.leaguesharing.service.model.entity;

import javax.persistence.*;

@Entity
public class Role extends BaseEntity {
	/**
	 * Member variables
	 *********************************************************************/
	
	@Column(nullable=false, unique=true)
	private String name;

	/**
	 * End of Member variables
	 **************************************************************/
	
	/**
	 * Constructor
	 **************************************************************************/
	
	public Role() {
		super();
	}
	
	public Role(String name) {
		super();
		this.name = name;
	}

	/**
	 * End of constructor
	 *******************************************************************/
	
	/**
	 * getters / setters
	 ********************************************************************/
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * End of getters / setters
	 *************************************************************/
	
	/**
	 * Methods
	 ********************************************************************/
	
	@Override
	public String toString() {
		return "Role [name=" + name + "]";
	}
	
	/**
	 * End of Methods
	 ********************************************************************/

}
