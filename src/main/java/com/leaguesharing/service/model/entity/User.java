package com.leaguesharing.service.model.entity;

import javax.persistence.*;

@Entity
public class User extends BaseEntity {
	/**
	 * Member variables
	 *********************************************************************/
	
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="ROLE_ID", nullable=false)
	private Role role;

	@Column(nullable=false, unique=true)
	private String pseudo;
	@Column(nullable=false, unique=true)
	private String email;
	
	@Column(nullable=false, unique=false)
	private String password;
	private int shareEmail;
	private String gender;
	
	@Column(columnDefinition = "text")
	private String description;

	private String urlImageProfil;
	
	/**
	 * End of Member variables
	 **************************************************************/
	
	/**
	 * Constructor
	 **************************************************************************/
	
	public User(Role role, String pseudo, String email, String password, String gender, int shareEmail, String description,String urlImageProfil ) {
		this.role=role;
		this.pseudo=pseudo;
		this.email=email;
		this.password=password;
		this.gender=gender;
		this.shareEmail=shareEmail;
		this.description=description;
		this.urlImageProfil=urlImageProfil;
	}
	
	public User() {
		super();
	}
	
	public User(User u) {
		this.setEmail(u.getEmail());
		this.setPassword(u.getPassword());
		this.setRole(u.getRole());
	}
	
	public User(Role role, String email, String password) {
		super();
		this.setRole(role);
		this.setEmail(email);
		this.setPassword(password);
	}
	
	/**
	 * End of constructor
	 *******************************************************************/
	
	/**
	 * getters / setters
	 ********************************************************************/
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

	
	/**
	 * End of getters / setters
	 *************************************************************/
	
	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getShareEmail() {
		return shareEmail;
	}

	public void setShareEmail(int shareEmail) {
		this.shareEmail = shareEmail;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Methods
	 ********************************************************************/
	
	@Override
	public String toString() {
		return "User [role=" + role + ", uid=" + getUid() + ", email=" + email + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrlImageProfil() {
		return urlImageProfil;
	}

	public void setUrlImageProfil(String urlImageProfil) {
		this.urlImageProfil = urlImageProfil;
	}

	/**
	 * End of Methods
	 ********************************************************************/
	
	
}
