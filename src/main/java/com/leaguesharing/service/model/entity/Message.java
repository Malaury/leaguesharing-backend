package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Message extends BaseEntity {

    @ManyToOne(cascade={CascadeType.PERSIST})
    @JoinColumn(name="OWNER_ID", nullable=false)
    private User writer;

    private String message;

    public Message(User writer, String message){
        this.message=message;
        this.writer=writer;
    }

    public Message(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getWriter() {
        return writer;
    }

    public void setWriter(User writer) {
        this.writer = writer;
    }
}
