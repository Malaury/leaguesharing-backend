package com.leaguesharing.service.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class CommentNoteArticle extends CommentNote{

	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="ARTICLE_ID", nullable=true)
	private Article article;
	
	public CommentNoteArticle(User writer, String comment, int note, Article article) {
		super();
		this.article=article;
	}
	
	public CommentNoteArticle() {
		
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
