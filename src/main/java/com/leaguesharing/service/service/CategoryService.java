package com.leaguesharing.service.service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.CategoryAdapter;
import com.leaguesharing.service.model.dao.CategoryDAO;
import com.leaguesharing.service.model.dto.CategoryDTO;
import com.leaguesharing.service.model.entity.Category;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("categoryService")
public class CategoryService extends AbstractSecurityService{

    private CategoryDAO categoryDAO;

    CategoryService(CategoryDAO categoryDAO){
        this.categoryDAO=categoryDAO;
    }

    public CategoryDTO getCategoryByUid(String uid) {
        return CategoryAdapter.toDTO(categoryDAO.findByUid(uid));}

     public ArrayList<CategoryDTO> getCategories() {
        return CategoryAdapter.toDTO(categoryDAO.findAll());
     }

     public CategoryDTO addCategory(CategoryDTO c, String uid) {
         Category cat = CategoryAdapter.toEntity((c));

         categoryDAO.save(cat);

         return CategoryAdapter.toDTO(cat);
     }

     public ArrayList<CategoryDTO> getCategoriesByParent(String uidParent){
        return CategoryAdapter.toDTO(categoryDAO.findByCategoryParentUid(uidParent));
     }

     public ArrayList<CategoryDTO> getCategoryChildren(){
        return CategoryAdapter.toDTO(categoryDAO.findByCategoryParentIsNotNull());
     }
     public  Long deleteByUid(String uid, String uidParent){
        Category c = categoryDAO.findByUid(uid);
        if(c!=null){
            if(c.getCategoryParent().getUid().equals(uidParent)){
                return categoryDAO.deleteByUid(uid);
            }
        }
        throw new AccessDeniedException();
     }

     public ArrayList<CategoryDTO> getBasicCategories(){
        return CategoryAdapter.toDTO(categoryDAO.findByCategoryParentIsNull());
     }
}
