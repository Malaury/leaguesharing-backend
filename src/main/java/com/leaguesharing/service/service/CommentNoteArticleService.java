package com.leaguesharing.service.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.CommentNoteArticleAdapter;
import com.leaguesharing.service.model.dao.ArticleDAO;
import com.leaguesharing.service.model.dao.CommentNoteArticleDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.CommentNoteArticleDTO;
import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.entity.CommentNote;
import com.leaguesharing.service.model.entity.CommentNoteArticle;

@Service("commentNoteArticleService")
public class CommentNoteArticleService extends AbstractSecurityService  {
	private UserDAO userDao;
	private ArticleDAO articleDao;
	private CommentNoteArticleDAO commentNoteArticleDao;


	public CommentNoteArticleService(UserDAO user, ArticleDAO article, CommentNoteArticleDAO commentNoteArticle){
		userDao=user;
		articleDao =article;
		commentNoteArticleDao= commentNoteArticle;
	}
	
	public CommentNoteArticleDTO getCommentNoteArticleByUid(String uid){
		return CommentNoteArticleAdapter.toDTO(commentNoteArticleDao.findByUid(uid));
	}
	
	public ArrayList<CommentNoteArticleDTO> getCommentNoteArticles(){
		return CommentNoteArticleAdapter.toDTO(commentNoteArticleDao.findAll());
	}
	
	public ArrayList<CommentNoteArticleDTO> getCommentNoteArticleByArticleUidAndWriterUid(String idArticle, String idWriter){
		return CommentNoteArticleAdapter.toDTO(commentNoteArticleDao.findByArticleUidAndWriterUid(idArticle,idWriter));
	}
	
	public CommentNoteArticleDTO addCommentNoteArticle(CommentNoteArticleDTO commentNoteArticle, String uUid) {
		CommentNoteArticle c = CommentNoteArticleAdapter.toEntity(commentNoteArticle);
		if(commentNoteArticle.getWriter().getUid().equals(uUid)) {
			c.setWriter(userDao.findByUid(commentNoteArticle.getWriter().getUid()));
			c.setArticle(articleDao.findByUid(commentNoteArticle.getArticle().getUid()));
			commentNoteArticleDao.save(c);
			return CommentNoteArticleAdapter.toDTO(c);
		}
		throw new AccessDeniedException();
	}
	
	public Long deleteByUid(String uid, String uUid) {
		CommentNoteArticle a = commentNoteArticleDao.findByUid(uid);
		if(a!= null) {
			if(a.getWriter().getUid().equals(uUid)) {
				return commentNoteArticleDao.deleteByUid(uid);
			}
		}
		throw new AccessDeniedException();
	}
	
	public ArrayList<CommentNoteArticleDTO> getCommentNoteByArticleUid(String uid){
		return CommentNoteArticleAdapter.toDTO(commentNoteArticleDao.findByArticleUid(uid));
	}
}
