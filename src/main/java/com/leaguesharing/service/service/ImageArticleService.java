package com.leaguesharing.service.service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.ImageArticleAdapter;
import com.leaguesharing.service.model.dao.ArticleDAO;
import com.leaguesharing.service.model.dao.ImageArticleDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.ArticleDTO;
import com.leaguesharing.service.model.dto.ImageArticleDTO;
import com.leaguesharing.service.model.entity.ImageArticle;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("imageArticleService")
public class ImageArticleService extends AbstractSecurityService {

    private ImageArticleDAO imageArticleDAO;
    private ArticleDAO articleDAO;

    public ImageArticleService(ImageArticleDAO imageArticleDAO, ArticleDAO articleDAO){
        this.imageArticleDAO=imageArticleDAO;
        this.articleDAO = articleDAO;
    }

    public ImageArticleDTO getImageArticleByUid(String uid) {
        return ImageArticleAdapter.toDTO(imageArticleDAO.findByUid(uid));
    }

    public ArrayList<ImageArticleDTO> getImageArticles() {
        return ImageArticleAdapter.toDTO(imageArticleDAO.findAll());
    }

    public ImageArticleDTO addArticle(ImageArticleDTO a, String uUid) {
        ImageArticle art = ImageArticleAdapter.toEntity(a);

        art.setArticle(articleDAO.findByUid(uUid));

        imageArticleDAO.save(art);

        return ImageArticleAdapter.toDTO(art);
    }

    public ArrayList<ImageArticleDTO> getImageArticlesByArticleUid(String uid){
        System.out.println("All imageArticles => " + imageArticleDAO.findAll());
        System.out.println("uid => " + uid);
        System.out.println("LISTE IMAGE_ARTICLE UID_ARTICLE SERVICE:"+ imageArticleDAO.findByArticleUid(uid));
        return ImageArticleAdapter.toDTO(imageArticleDAO.findByArticleUid(uid));
    }

    public Long deleteByUid(String uid, String uUid) {
        ImageArticle a = imageArticleDAO.findByUid(uid);
        if(a != null) {
            if(a.getArticle().getUid().equals(uUid)) {
                return imageArticleDAO.deleteByUid(uid);
            }
        }
        throw new AccessDeniedException();
    }
}
