package com.leaguesharing.service.service;

import com.leaguesharing.service.model.dto.*;
import com.leaguesharing.service.model.dao.*;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.*;
import com.leaguesharing.service.model.entity.*;
import com.leaguesharing.service.model.adapter.*;

@Service("articleService")
public class ArticleService extends AbstractSecurityService {
	
	private ArticleDAO articleDao;
	private UserDAO userDao;
	private CategoryDAO categoryDAO;
	
	ArticleService(ArticleDAO article, UserDAO user, CategoryDAO category){
		articleDao=article;
		userDao=user;
		categoryDAO=category;
	}
	
	public ArticleDTO getArticleByUid(String uid) {
		return ArticleAdapter.toDTO(articleDao.findByUid(uid));
	}
	
	public ArrayList<ArticleDTO> getArticles() {
		return ArticleAdapter.toDTO(articleDao.findAll());
	}
	
	public ArticleDTO addArticle(ArticleDTO a, String uUid) {
		Article art = ArticleAdapter.toEntity(a);
		
		art.setUser(userDao.findByUid(uUid));

		art.setCategory(categoryDAO.findByUid(a.getCategory().getUid()));
		articleDao.save(art);
		
		return ArticleAdapter.toDTO(art);
	}
	
	public ArrayList<ArticleDTO> getArticlesByType(String type){
		return ArticleAdapter.toDTO(articleDao.findByType(type));
	}
	
	public ArrayList<ArticleDTO> getArticlesByUserUid(String uid){
		System.out.println("All articles => " + articleDao.findAll());
		System.out.println("uid => " + uid);
		System.out.println("LISTE ARTICLE UIDUSER SERVICE:"+ articleDao.findByUserUid(uid));
		return ArticleAdapter.toDTO(articleDao.findByUserUid(uid));
	}
	
	public Long deleteByUid(String uid, String uUid) {
		Article a = articleDao.findByUid(uid);
		if(a != null) {
			if(a.getUser().getUid().equals(uUid)) {
				return articleDao.deleteByUid(uid);
			}
		}
		throw new AccessDeniedException();
	}
}
