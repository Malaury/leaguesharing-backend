package com.leaguesharing.service.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.FavoryAdapter;
import com.leaguesharing.service.model.adapter.LikeUserAdapter;
import com.leaguesharing.service.model.dao.LikeUserDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.FavoryDTO;
import com.leaguesharing.service.model.dto.LikeUserDTO;
import com.leaguesharing.service.model.entity.Favory;
import com.leaguesharing.service.model.entity.LikeUser;

@Service("likeUserService")
public class LikeUserService {
	
	private UserDAO userDao;
	private LikeUserDAO likeUserDAO;
	
	LikeUserService(UserDAO userDAO, LikeUserDAO likeUserDAO){
		this.userDao=userDAO;
		this.likeUserDAO=likeUserDAO;
	}
	
	public LikeUserDTO getLikeUserByUid(String uid) {
		return LikeUserAdapter.toDTO(likeUserDAO.findByUid(uid));
	}
	
	public ArrayList<LikeUserDTO> getLikeUserByUserLikedUid(String uid) {
		return LikeUserAdapter.toDTO(likeUserDAO.findByUserLikedUid(uid));
	}
	
	public LikeUserDTO getLikeUserByUserLikedUidAndUserUid(String idUserLiked, String idUser){
		return LikeUserAdapter.toDTO(likeUserDAO.findByUserLikedUidAndUserUid(idUserLiked, idUser));
	}
	
	public LikeUserDTO addLikeUser(LikeUserDTO f, String uUid) {
		LikeUser lu = LikeUserAdapter.toEntity(f);
		if(f.getUser().getUid().equals(uUid)) {
			lu.setUser(userDao.findByUid(f.getUser().getUid()));
			lu.setUserLiked(userDao.findByUid(f.getUserLiked().getUid()));
			
			likeUserDAO.save(lu);
			
			return LikeUserAdapter.toDTO(lu);
		}
		throw new AccessDeniedException();
	}
	
	public Long deleteByUid(String uid, String uUid) {
		LikeUser l = likeUserDAO.findByUid(uid);
		if(l != null) {
			if(l.getUser().getUid().equals(uUid)) {
				return likeUserDAO.deleteByUid(uid);
			}
		}
		throw new AccessDeniedException();
	}
}
