package com.leaguesharing.service.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.CommentNoteArticleAdapter;
import com.leaguesharing.service.model.adapter.CommentNoteUserAdapter;
import com.leaguesharing.service.model.dao.ArticleDAO;
import com.leaguesharing.service.model.dao.CommentNoteUserDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.CommentNoteArticleDTO;
import com.leaguesharing.service.model.dto.CommentNoteDTO;
import com.leaguesharing.service.model.dto.CommentNoteUserDTO;
import com.leaguesharing.service.model.entity.CommentNoteArticle;
import com.leaguesharing.service.model.entity.CommentNoteUser;

@Service("commentNoteUserService")
public class CommentNoteUserService extends AbstractSecurityService  {

	private UserDAO userDao;
	private ArticleDAO articleDao;
	private CommentNoteUserDAO commentNoteUserDao;
	
	public CommentNoteUserService(UserDAO user, ArticleDAO article, CommentNoteUserDAO commentNoteUser){
		userDao=user;
		articleDao =article;
		commentNoteUserDao= commentNoteUser;
	}
	
	public CommentNoteUserDTO getCommentNoteUserByUid(String uid){
		return CommentNoteUserAdapter.toDTO(commentNoteUserDao.findByUid(uid));
	}
	
	public ArrayList<CommentNoteUserDTO> getCommentNoteUsers(){
		return CommentNoteUserAdapter.toDTO(commentNoteUserDao.findAll());
	}
	
	public CommentNoteUserDTO addCommentNoteUser(CommentNoteUserDTO commentNoteUser, String uUid) {
		CommentNoteUser c = CommentNoteUserAdapter.toEntity(commentNoteUser);
		if(commentNoteUser.getWriter().getUid().equals(uUid)) {
			c.setWriter(userDao.findByUid(commentNoteUser.getWriter().getUid()));
			c.setRecipient(userDao.findByUid(commentNoteUser.getRecipient().getUid()));
			commentNoteUserDao.save(c);
			return CommentNoteUserAdapter.toDTO(c);
		}
		throw new AccessDeniedException();
	}
	
	public Long deleteByUid(String uid, String uUid) {
		CommentNoteUser a = commentNoteUserDao.findByUid(uid);
		if( a != null) {
			if(a.getWriter().getUid().equals(uUid)){
				return commentNoteUserDao.deleteByUid(uid);
			}
		}
		throw new AccessDeniedException();
	}
	
	public ArrayList<CommentNoteUserDTO> getCommentNoteByRecipientUid(String uid){
		return CommentNoteUserAdapter.toDTO(commentNoteUserDao.findByRecipientUid(uid));
	}
	
	public ArrayList<CommentNoteUserDTO> getCommentNoteUserByUserIdAndRecipientId(String idUser, String idRecipient){
		return CommentNoteUserAdapter.toDTO(commentNoteUserDao.findByWriterUidAndRecipientUid(idUser,idRecipient));
	}
	
}
