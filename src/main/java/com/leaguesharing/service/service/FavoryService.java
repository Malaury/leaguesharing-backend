package com.leaguesharing.service.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.adapter.FavoryAdapter;
import com.leaguesharing.service.model.dao.ArticleDAO;
import com.leaguesharing.service.model.dao.FavoryDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.FavoryDTO;
import com.leaguesharing.service.model.entity.Favory;

@Service("favoryService")
public class FavoryService extends AbstractSecurityService {

		private FavoryDAO favoryDao;
		private ArticleDAO articleDao;
		private UserDAO userDao;
		
		FavoryService(FavoryDAO favory, ArticleDAO article, UserDAO user){
			favoryDao=favory;
			articleDao=article;
			userDao=user;
		}
		
		public FavoryDTO getFavoryByUid(String uid) {
			return FavoryAdapter.toDTO(favoryDao.findByUid(uid));
		}
		
		public ArrayList<FavoryDTO> getFavories(){
			return FavoryAdapter.toDTO(favoryDao.findAll());
		}
		
		public FavoryDTO getFavoryByArticleAndUser(String idArticle, String idUser, String uUid){
			if(idUser.equals(uUid)) {
				System.out.println(idArticle + " -> " + idUser);
				return FavoryAdapter.toDTO(favoryDao.findByUserUidAndArticleUid(idUser, idArticle));
			}
			throw new AccessDeniedException();
		}
		
		public FavoryDTO addFavory(FavoryDTO f, String uUid) {
			Favory fav = FavoryAdapter.toEntity(f);
			if(f.getUser().getUid().equals(uUid)) {
				fav.setArticle(articleDao.findByUid(f.getArticle().getUid()));
				fav.setUser(userDao.findByUid(f.getUser().getUid()));
				
				favoryDao.save(fav);
				
				return FavoryAdapter.toDTO(fav);
			}
			throw new AccessDeniedException();
		}
		
		public Long deleteByUid(String uid, String uUid) {
			Favory f = favoryDao.findByUid(uid);
			if(f != null ) {
				if(f.getUser().getUid().equals(uUid)) {
					return favoryDao.deleteByUid(uid);
				}
			}
			throw new AccessDeniedException();
		}
		
		public ArrayList<FavoryDTO> getFavoryByUserUid(String uid, String uUid){
			if(uid.equals(uUid)) {
				return FavoryAdapter.toDTO(favoryDao.findByUserUid(uid));
			}
			throw new AccessDeniedException();
		}
}
