package com.leaguesharing.service.service;
import java.util.ArrayList;

import org.apache.tomcat.util.log.SystemLogHandler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.leaguesharing.service.exception.AccessDeniedException;
import com.leaguesharing.service.model.*;
import com.leaguesharing.service.model.adapter.UserAdapter;
import com.leaguesharing.service.model.dao.RoleDAO;
import com.leaguesharing.service.model.dao.UserDAO;
import com.leaguesharing.service.model.dto.UserDTO;
import com.leaguesharing.service.model.entity.User;

@Service("userService")
public class UserService extends AbstractSecurityService {
	
	private UserDAO userDao;
	private RoleDAO roleDao;
	private PasswordEncoder passwordEncoder;
	
	UserService(UserDAO user, RoleDAO role, PasswordEncoder passwordEncoder){
		userDao=user;
		roleDao= role;
		this.passwordEncoder = passwordEncoder;
	}
	
	public UserDTO getUserByUid(String uid) {
		return UserAdapter.toDTO(userDao.findByUid(uid));
	}
	
	public UserDTO getUserByPseudo(String pseudo) {
		return UserAdapter.toDTO(userDao.findByPseudo(pseudo));
	}
	
	public UserDTO getUserByEmail(String email) {
		return UserAdapter.toDTO(userDao.findByEmail(email));
	}
	
	public ArrayList<UserDTO> getUsers(){
		return UserAdapter.toDTO(userDao.findAll());
	}
	
	public User getUser(UserDTO user) {
		return UserAdapter.toEntity(user);
	}
	
	public UserDTO addUser(UserDTO user) {
		User u = UserAdapter.toEntity(user);
		
		if(u.getRole() == null) {
			u.setRole(roleDao.findByName("PARTICULAR"));
		} else {
			u.setRole(roleDao.findByName(u.getRole().getName()));
		}
		u.setDescription(u.getDescription());
		u.setPassword(passwordEncoder.encode(u.getPassword()));
		
		userDao.save(u);
		
		return UserAdapter.toDTO(u);
	}
	
	public UserDTO updateUser(UserDTO user, String uUid) {
		if(user.getUid().equals(uUid)) {
			User u = userDao.findByUid(user.getUid());
			u.setDescription(user.getDescription());
			u.setEmail(user.getEmail());
			u.setGender(user.getGender());
			userDao.save(u);
			return UserAdapter.toDTO(u);
		}
		throw new AccessDeniedException();
	}
	
	public UserDTO updatePassword(UserDTO user, String uUid) {
		if(user.getUid().equals(uUid)) {
			User u = userDao.findByUid(user.getUid());
			u.setPassword(passwordEncoder.encode(user.getPassword()));
			userDao.save(u);
			return UserAdapter.toDTO(u);
		}
		throw new AccessDeniedException();
	}
}
