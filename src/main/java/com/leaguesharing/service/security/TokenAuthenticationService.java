package com.leaguesharing.service.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.leaguesharing.service.exception.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import java.io.IOException;

class TokenAuthenticationService {
	// Member variables
	// --------------------------------------------------------------------------------------------------------
	static final long EXPIRATIONTIME_JWT = 3_600_000; // 1 hour
	static final long EXPIRATIONTIME_REFRESH = 604_800_000; // 1 week
	static final String CLIENT_ID = "G9BQGG1fFnA8pPKz5iDysVNbiNjKwF6f";
	static final String CLIENT_SECRET = "BBpuufyiVgp4pu0mo5Yi1WtStSU1pmwe";
	static final String SECRET_ACCESS = "gSukLvsCYmAp7M1k6rQm0IkQUKY7BPyA";
	static final String SECRET_REFRESH = "3yJwNvQbuMRuN5VYcwcItfB0qQCUrrqL";
	static final String ISSUER = "p7u8rE5O1LOyJDRVL4KB3ncCVe6OB5kM";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER = "Authorization";
	// End of Member variables
	// -------------------------------------------------------------------------------------------------

	// Methods
	// -----------------------------------------------------------------------------------------------------------------
	static String addAuthentication(HttpServletResponse res, CustomUserDetails user) throws IOException {
		Claims claimsAccess = Jwts.claims().setSubject(user.getEmail());
		claimsAccess.put("scopes", user.getRole().getName());
		claimsAccess.put("uid", user.getUid());
		System.err.println(user.getPseudo());
		claimsAccess.put("pseudo", user.getPseudo());
		
		System.err.println("gen token");
		
		String JWT_ACCESS = Jwts.builder().setClaims(claimsAccess).setIssuer(ISSUER)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME_JWT))
				.signWith(SignatureAlgorithm.HS512, SECRET_ACCESS).compact();

		Claims claimsRefresh = Jwts.claims().setSubject(user.getEmail());
		claimsRefresh.put("scopes", "REFRESH_TOKEN");

		String refresh_uid = UUID.randomUUID().toString();
		String JWT_REFRESH = Jwts.builder().setClaims(claimsRefresh).setIssuer(ISSUER)
				.setId(refresh_uid).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME_REFRESH))
				.signWith(SignatureAlgorithm.HS512, SECRET_REFRESH).compact();
		
		res.getWriter().append("{\"ACCESS_TOKEN\":\"" + JWT_ACCESS + "\",\"REFRESH_TOKEN\":\"" + JWT_REFRESH + "\"}");
		
		return refresh_uid;
	}

	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER);
		
		if(token == null || token.isEmpty()) {
			token = request.getParameter(HEADER);
		}
		
		if (token != null && !token.isEmpty()) { // parse the token.
			try {
				Claims jwt = Jwts.parser().setSigningKey(SECRET_ACCESS).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();
				
				if (jwt != null && jwt.getSubject() != null && jwt.get("scopes") != null && jwt.get("uid") != null) {
					ArrayList<GrantedAuthority> lA = new ArrayList<GrantedAuthority>();
					lA.add(new SimpleGrantedAuthority(jwt.get("scopes").toString().toUpperCase()));
					
					UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(jwt.getSubject(), null, lA);
					upat.setDetails(jwt.get("uid"));
					
					return upat;
				}
			} catch (Exception ex) {
				throw new AccessDeniedException();
			}
		}
		return null;
	}
	// End of Methods
	// ----------------------------------------------------------------------------------------------------------
}