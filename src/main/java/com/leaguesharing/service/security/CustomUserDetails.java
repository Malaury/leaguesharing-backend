package com.leaguesharing.service.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.leaguesharing.service.model.entity.User;

@SuppressWarnings("serial")
public class CustomUserDetails extends User implements UserDetails {	

	public CustomUserDetails(User user){
		super(user);
		this.setUid(user.getUid());
		this.setEmail(user.getEmail());
		this.setPseudo(user.getPseudo());
		this.setRole(user.getRole());
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {			
		return AuthorityUtils.commaSeparatedStringToAuthorityList(this.getRole().getName());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUsername() {
		return this.getEmail();
	}
}
