package com.leaguesharing.service.security;

import com.leaguesharing.service.model.entity.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
	static final Logger logger = LoggerFactory.getLogger(JWTLoginFilter.class);
	private final UserDetailsService userDetailsService;
	
	// Methods
	// ------------------------------------------------------------------------------------------------------------------
	public JWTLoginFilter(String url, AuthenticationManager authManager, UserDetailsService userDetailsService) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
		this.userDetailsService = userDetailsService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException, IOException, ServletException {
		
		logger.info("attemptAuth with password");
		
		if(req.getParameter("grant_type") != null && req.getParameter("grant_type").equalsIgnoreCase("password")) {
			if(req.getParameter("client_id") != null && req.getParameter("client_id").equals(TokenAuthenticationService.CLIENT_ID)) {
				if(req.getParameter("client_secret") != null && req.getParameter("client_secret").equals(TokenAuthenticationService.CLIENT_SECRET)) {
					
					String username = req.getParameter("username");
					String password = req.getParameter("password");
					
					logger.info(username + " => " + password);
					if(username != null && password != null) {	
						User u = new User(null, username, password);
						CustomUserDetails creds = new CustomUserDetails(u);
	
						return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(creds.getEmail(),
								creds.getPassword(), Collections.emptyList()));
					}
				}
			}
		}
		
		return null;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		String tokenUid = TokenAuthenticationService.addAuthentication(res,
				(CustomUserDetails) userDetailsService.loadUserByUsername(auth.getName()));
		
		((CustomUserDetailsService)userDetailsService).addRefreshToken(auth.getName(), tokenUid);
	}
	// End of Methods
	// ----------------------------------------------------------------------------------------------------------
}