package com.leaguesharing.service.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.leaguesharing.service.model.dao.*;
import com.leaguesharing.service.model.entity.*;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	UserDAO userDAO;
	UserRefreshTokenDAO userRefreshTokenDAO;

	public CustomUserDetailsService(UserDAO userDAO, UserRefreshTokenDAO userRefreshTokenDAO) {
		this.userDAO = userDAO;
		this.userRefreshTokenDAO = userRefreshTokenDAO;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDAO.findByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("No user present with email: " + username);
		}
		
		return new CustomUserDetails(user);
	}
	
	public void disableOldToken(String username) {
		UserRefreshToken urt = userRefreshTokenDAO.findByEmail(username);
		if(urt != null) {
			userRefreshTokenDAO.delete(urt);
		}
	}
	
	public void addRefreshToken(String username, String tokenUid) {
		// Disable all old token...
		disableOldToken(username);
		// Add new token...
		UserRefreshToken urt = new UserRefreshToken(username, tokenUid);
		userRefreshTokenDAO.save(urt);
	}
	
	public Boolean isEnableToken(String username, String tokenUid) {
		UserRefreshToken urt = userRefreshTokenDAO.findByEmailAndTokenUid(username, tokenUid);
		if(urt != null) {
			return urt.getEnable();
		}
		
		return false;
	}
}