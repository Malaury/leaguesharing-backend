package com.leaguesharing.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.leaguesharing.service.exception.AccessDeniedException;
import io.jsonwebtoken.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.util.Collections.emptyList;

import java.io.IOException;

public class JWTRefreshFilter extends AbstractAuthenticationProcessingFilter {
	static final Logger logger = LoggerFactory.getLogger(JWTRefreshFilter.class);
	private final UserDetailsService userDetailsService;
	
	// Methods
	// ------------------------------------------------------------------------------------------------------------------
	public JWTRefreshFilter(String url, AuthenticationManager authManager, UserDetailsService userDetailsService) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
		this.userDetailsService = userDetailsService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException, IOException, ServletException {
		
		logger.info("attemptAuth with refreshToken");
		
		if(req.getParameter("grant_type") != null && req.getParameter("grant_type").equalsIgnoreCase("refresh_token")) {
			if(req.getParameter("client_id") != null && req.getParameter("client_id").equals(TokenAuthenticationService.CLIENT_ID)) {
				if(req.getParameter("client_secret") != null && req.getParameter("client_secret").equals(TokenAuthenticationService.CLIENT_SECRET)) {
					
					String refreshToken_string = req.getParameter("refresh_token");
					if(refreshToken_string != null) {
						try {
							String tokenUid = Jwts.parser().setSigningKey(TokenAuthenticationService.SECRET_REFRESH).parseClaimsJws(refreshToken_string).getBody().getId();
							String username = Jwts.parser().setSigningKey(TokenAuthenticationService.SECRET_REFRESH).parseClaimsJws(refreshToken_string).getBody().getSubject();
							
							if(((CustomUserDetailsService)userDetailsService).isEnableToken(username, tokenUid)) {
								CustomUserDetails creds = (CustomUserDetails) userDetailsService.loadUserByUsername(username);
								return new UsernamePasswordAuthenticationToken(creds, null, emptyList());
							}
						} catch (Exception ex) {
							logger.info("Error refreshToken :" + ex);
							throw new AccessDeniedException();
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		String tokenUid = TokenAuthenticationService.addAuthentication(res,
				(CustomUserDetails) userDetailsService.loadUserByUsername(auth.getName()));
		
		((CustomUserDetailsService)userDetailsService).addRefreshToken(auth.getName(), tokenUid);
	}
	// End of Methods
	// ----------------------------------------------------------------------------------------------------------
}